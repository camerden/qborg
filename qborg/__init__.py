# QBorg metadata

__description__ = 'An easy to use encrypting, deduplicating backup software.'

__author__ = 'The QBorg team'
__copyright__ = 'Copyright 2018 %s' % __author__
__license__ = 'GPLv3'
__maintainer__ = __author__
# Versions as per PEP 440 (https://www.python.org/dev/peps/pep-0440/)
__version_info__ = (0, 0, 1)
__version_suffix__ = 'dev1'
__version__ = '.'.join(str(x) for x in (*__version_info__, __version_suffix__))
