<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_CH" sourcelanguage="en_GB">
<context>
    <name>CreateBackupGui</name>
    <message>
        <location filename="createbackup.py" line="105"/>
        <source>Input error</source>
        <translation>Eingabefehler</translation>
    </message>
    <message>
        <location filename="createbackup.py" line="112"/>
        <source>Successfully created backup</source>
        <translation>Backup wurde erfolgreich erstellt</translation>
    </message>
    <message>
        <location filename="../guis/createbackup.py" line="114"/>
        <source>Please select compression methode</source>
        <translation>Bitte wählen Sie einen Kompressionsmodus</translation>
    </message>
</context>
<context>
    <name>ExtractBackupGui</name>
    <message>
        <location filename="extractbackup.py" line="23"/>
        <source>Choose Repository</source>
        <translation>Repository wählen</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="39"/>
        <source>Please choose a path to restore the backup</source>
        <translation>Bitte wähle einen Pfad, um das Backup dort wiederherzustellen</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="42"/>
        <source>Input error</source>
        <translation>Eingabefehler</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="48"/>
        <source>Successfully restored backup</source>
        <translation>Backup erfolgreich wiederhergestellt</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="51"/>
        <source>Error restoring backup</source>
        <translation>Fehler beim Wiederherstellen des Backups</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="54"/>
        <source>Repository Password</source>
        <translation>Repository Passwort</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="54"/>
        <source>Enter repository password</source>
        <translation>Gebe das Repository Passwort ein</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="initrepo.ui" line="14"/>
        <source>New repository</source>
        <translation>Neues Repository</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="86"/>
        <source>Path:</source>
        <translation>Pfad:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="63"/>
        <source>Encryption method:</source>
        <translation>Verschlüsselungsmethode:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="93"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="56"/>
        <source>Password:</source>
        <translation>Passwort:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="73"/>
        <source>Password check:</source>
        <translation>Passwort erneut:</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="14"/>
        <source>Create Backup</source>
        <translation>Backup erstellen</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="45"/>
        <source>Choose Repository:</source>
        <translation>Repository auswählen:</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="32"/>
        <source>Previous Jobs as Template:</source>
        <translation>Ausgeführte Jobs als Template:</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="75"/>
        <source>Directories and Files</source>
        <translation>Ordner und Dokumente</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="81"/>
        <source>Add Directory</source>
        <translation>Füge Ordner hinzu</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="91"/>
        <source>Add Files</source>
        <translation>Füge Dokument hinzu</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="98"/>
        <source>Remove marked Directory or File</source>
        <translation>Entferne markierten Ordner oder File</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="106"/>
        <source>Advanced</source>
        <translation>Vorangeschritten</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="112"/>
        <source>PushButton</source>
        <translation>PushButton</translation>
    </message>
    <message>
        <location filename="../forms/rename_repo.ui" line="14"/>
        <source>Rename Repository</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/rename_repo.ui" line="29"/>
        <source>Repository name:</source>
        <translation>Repository-Name:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="100"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="107"/>
        <source>Backing Store:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InitRepoGui</name>
    <message>
        <location filename="../guis/initrepo.py" line="62"/>
        <source>Choose Repository</source>
        <translation>Repository wählen</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="90"/>
        <source>Please choose a path for the repository</source>
        <translation>Bitte wähle einen Pfad für das Repository</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="103"/>
        <source>Input error</source>
        <translation>Eingabefehler</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="96"/>
        <source>Please enter a password</source>
        <translation>Bitte gib ein Passwort ein</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="98"/>
        <source>Please repeat the password</source>
        <translation>Bitte gib das Passwort erneut ein</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="100"/>
        <source>Passwords do not match</source>
        <translation>Die Passwörter stimmen nicht überein</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="109"/>
        <source>Successfully created repository</source>
        <translation>Repository erfolgreich erstellt</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="112"/>
        <source>Error creating repository</source>
        <translation>Fehler bei der Repository-Erstellung</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="88"/>
        <source>The repository name is invalid:
%s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainGui</name>
    <message>
        <location filename="main.py" line="60"/>
        <source>Successfully deleted repository</source>
        <translation>Das Repository konnte erfolgreich gelöscht werden</translation>
    </message>
    <message>
        <location filename="main.py" line="63"/>
        <source>Error deleting repository</source>
        <translation>Beim Löschen des Repositories ist ein Fehler aufgetreten</translation>
    </message>
    <message>
        <location filename="main.py" line="66"/>
        <source>Successfully renamed backup</source>
        <translation>Backup erfolgreich umbenannt</translation>
    </message>
    <message>
        <location filename="main.py" line="69"/>
        <source>Error renaming backup</source>
        <translation>Fehler beim Umbennen des Backups</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="main.ui" line="14"/>
        <source>QBorg</source>
        <translation>QBorg</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="144"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="153"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="159"/>
        <source>Help</source>
        <translation>Hilfe</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="170"/>
        <source>Close</source>
        <translation>Schliessen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="76"/>
        <source>Delete Repository</source>
        <translation>Repository löschen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="83"/>
        <source>Create Backup</source>
        <translation>Backup erstellen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="90"/>
        <source>Delete Backup</source>
        <translation>Backup löschen</translation>
    </message>
    <message>
        <location filename="main.ui" line="65"/>
        <source>Create Repository</source>
        <translation>Repository erstellen</translation>
    </message>
    <message utf8="true">
        <location filename="main.ui" line="137"/>
        <source>New Repository…</source>
        <translation type="obsolete">Neues Repository...</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="180"/>
        <source>Preferences…</source>
        <translation>Einstellungen...</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="185"/>
        <source>About QBorg…</source>
        <translation>Über QBorg...</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="97"/>
        <source>Restore Backup</source>
        <translation>Backup wiederherstellen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="104"/>
        <source>Create Job</source>
        <translation>Job erstellen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="111"/>
        <source>Delete Job</source>
        <translation>Job löschen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="118"/>
        <source>Run Job</source>
        <translation>Job ausführen</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="175"/>
        <source>Create Repository…</source>
        <translation>Repository erstellen</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="190"/>
        <source>Create Backup…</source>
        <translation>Backup erstellen</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="193"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="198"/>
        <source>Rename Repository</source>
        <translation>Repository umbenennen</translation>
    </message>
</context>
<context>
    <name>QBorgGui</name>
    <message>
        <location filename="qborggui.py" line="34"/>
        <source>Success</source>
        <translation>Erfolg</translation>
    </message>
    <message>
        <location filename="qborggui.py" line="60"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
</context>
<context>
    <name>RenameRepoGui</name>
    <message>
        <location filename="renamerepo.py" line="23"/>
        <source>Please choose a new name for the repository</source>
        <translation type="obsolete">Bitte einen Namen für das Repository eingeben</translation>
    </message>
    <message>
        <location filename="../guis/renamerepo.py" line="34"/>
        <source>Input error</source>
        <translation>Eingabefehler</translation>
    </message>
    <message>
        <location filename="../guis/renamerepo.py" line="27"/>
        <source>Please choose a name for the repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guis/renamerepo.py" line="31"/>
        <source>Name already exists</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>self.parent</name>
    <message>
        <location filename="deleterepocontroller.py" line="15"/>
        <source>Deleting repository...</source>
        <translation>Repository wird gelöscht...</translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="58"/>
        <source>Delete Repository</source>
        <translation>Repository löschen</translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="58"/>
        <source>You requested to completely DELETE the repository *including* all archives it contain. Do you want to continue?</source>
        <translation>Sie haben darum gebeten, das Repository *inklusive* aller darin enthaltenen Archive vollständig zu löschen. Wollen Sie weitermachen?</translation>
    </message>
    <message>
        <location filename="deletebackupcontroller.py" line="47"/>
        <source>Delete Backup</source>
        <translation>Backup löschen</translation>
    </message>
    <message>
        <location filename="deletebackupcontroller.py" line="47"/>
        <source>Do you really want to delete this archive?</source>
        <translation>Wollen Sie wirklich dieses Backup löschen?</translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="49"/>
        <source>You requested to completely DELETE the repository
*including* all archives it contains.

 Please confirm by entering the repository password</source>
        <translation>Sie versuchen gerade das komplette Repository zu LÖSCHEN *inklusive* allen Backups darin

Bitte bestätigen Sie diesen Vorgang mit der Eingabe des Repository Passworts</translation>
    </message>
</context>
</TS>
