<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="rm_CH" sourcelanguage="en_GB">
<context>
    <name></name>
    <message>
        <location filename="msgutil.py" line="14"/>
        <source>Error</source>
        <translation type="obsolete">Errur</translation>
    </message>
</context>
<context>
    <name>BackUpGui</name>
    <message>
        <location filename="gui.py" line="135"/>
        <source>Choose Repository</source>
        <translation type="obsolete">Selecziun dal recipient</translation>
    </message>
    <message>
        <location filename="gui.py" line="161"/>
        <source>Please choose a path for the repository</source>
        <translation type="obsolete">Selecziunavas in lieu per il recipient, per plaschair</translation>
    </message>
    <message>
        <location filename="gui.py" line="173"/>
        <source>Input error</source>
        <translation type="obsolete">Errur d&apos;endataziun</translation>
    </message>
</context>
<context>
    <name>CreateBackupGui</name>
    <message>
        <location filename="../guis/createbackup.py" line="108"/>
        <source>Please choose a path for the repository</source>
        <translation>Selecziunavas in lieu per il recipient, per plaschair</translation>
    </message>
    <message>
        <location filename="createbackup.py" line="112"/>
        <source>Successfully created backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guis/createbackup.py" line="114"/>
        <source>Please select compression methode</source>
        <translation>Selecziunavas in modus da compressiun, per plaschair</translation>
    </message>
</context>
<context>
    <name>ExtractBackupGui</name>
    <message>
        <location filename="extractbackup.py" line="23"/>
        <source>Choose Repository</source>
        <translation type="unfinished">Selecziun dal recipient</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="39"/>
        <source>Please choose a path to restore the backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="42"/>
        <source>Input error</source>
        <translation>Errur d&apos;endataziun</translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="48"/>
        <source>Successfully restored backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="51"/>
        <source>Error restoring backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="54"/>
        <source>Repository Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="extractbackup.py" line="54"/>
        <source>Enter repository password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="initrepo.ui" line="14"/>
        <source>New repository</source>
        <translation>Nov recipient</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="86"/>
        <source>Path:</source>
        <translation>Lieu:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="63"/>
        <source>Encryption method:</source>
        <translation type="obsolete">Metoda da codaziun:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="93"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="56"/>
        <source>Password:</source>
        <translation>Pled-clav:</translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="73"/>
        <source>Password check:</source>
        <translation>Pled-clav (repetiziun):</translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="14"/>
        <source>Create Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="45"/>
        <source>Choose Repository:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="32"/>
        <source>Previous Jobs as Template:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="75"/>
        <source>Directories and Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="81"/>
        <source>Add Directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="91"/>
        <source>Add Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="98"/>
        <source>Remove marked Directory or File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="106"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="create_backup.ui" line="112"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/rename_repo.ui" line="14"/>
        <source>Rename Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/rename_repo.ui" line="29"/>
        <source>Repository name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="100"/>
        <source>Name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/initrepo.ui" line="107"/>
        <source>Backing Store:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InitRepoGui</name>
    <message>
        <location filename="../guis/initrepo.py" line="62"/>
        <source>Choose Repository</source>
        <translation>Selecziun dal recipient</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="90"/>
        <source>Please choose a path for the repository</source>
        <translation>Selecziunavas in lieu per il recipient, per plaschair</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="103"/>
        <source>Input error</source>
        <translation>Errur d&apos;endataziun</translation>
    </message>
    <message>
        <location filename="../gui.py" line="89"/>
        <source>Error</source>
        <translation type="obsolete">Errur</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="96"/>
        <source>Please enter a password</source>
        <translation>Endatavas in pled-clav, per plaschair</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="98"/>
        <source>Please repeat the password</source>
        <translation>Repetivas il pled-clav, per plaschair</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="100"/>
        <source>Passwords do not match</source>
        <translation>Ils pled-clavs na correspundan</translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="109"/>
        <source>Successfully created repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="112"/>
        <source>Error creating repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guis/initrepo.py" line="88"/>
        <source>The repository name is invalid:
%s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainGui</name>
    <message>
        <location filename="main.py" line="60"/>
        <source>Successfully deleted repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="63"/>
        <source>Error deleting repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="66"/>
        <source>Successfully renamed backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.py" line="69"/>
        <source>Error renaming backup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context encoding="UTF-8">
    <name>MainWindow</name>
    <message>
        <location filename="main.ui" line="14"/>
        <source>QBorg</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="144"/>
        <source>File</source>
        <translation>Datoteca</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="153"/>
        <source>Edit</source>
        <translation>Modifitgar</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="159"/>
        <source>Help</source>
        <translation>Agid</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="170"/>
        <source>Close</source>
        <translation>Serrar</translation>
    </message>
    <message utf8="true">
        <location filename="main.ui" line="137"/>
        <source>New Repository…</source>
        <translation type="obsolete">Nov recipient…</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="180"/>
        <source>Preferences…</source>
        <translation>Configuraziuns…</translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="185"/>
        <source>About QBorg…</source>
        <translation>Tge è QBorg?</translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="76"/>
        <source>Delete Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="83"/>
        <source>Create Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="90"/>
        <source>Delete Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="main.ui" line="65"/>
        <source>Create Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="97"/>
        <source>Restore Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="104"/>
        <source>Create Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="111"/>
        <source>Delete Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="118"/>
        <source>Run Job</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="175"/>
        <source>Create Repository…</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <location filename="../forms/main.ui" line="190"/>
        <source>Create Backup…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="193"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../forms/main.ui" line="198"/>
        <source>Rename Repository</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QBorgGui</name>
    <message>
        <location filename="qborggui.py" line="34"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="qborggui.py" line="60"/>
        <source>Error</source>
        <translation type="unfinished">Errur</translation>
    </message>
</context>
<context>
    <name>RenameRepoGui</name>
    <message>
        <location filename="../guis/renamerepo.py" line="34"/>
        <source>Input error</source>
        <translation type="unfinished">Errur d&apos;endataziun</translation>
    </message>
    <message>
        <location filename="../guis/renamerepo.py" line="27"/>
        <source>Please choose a name for the repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../guis/renamerepo.py" line="31"/>
        <source>Name already exists</source>
    </message>
</context>
    <name>self.parent</name>
    <message>
        <location filename="deleterepocontroller.py" line="15"/>
        <source>Deleting repository...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="58"/>
        <source>Delete Repository</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="58"/>
        <source>You requested to completely DELETE the repository *including* all archives it contain. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="deletebackupcontroller.py" line="47"/>
        <source>Delete Backup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="deletebackupcontroller.py" line="47"/>
        <source>Do you really want to delete this archive?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="deleterepocontroller.py" line="49"/>
        <source>You requested to completely DELETE the repository 
*including* all archives it contains.

 Please confirm by entering the repository password</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
