.. QBorg documentation master file, created by
   sphinx-quickstart on Mon Mar 26 17:31:38 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to QBorg's documentation!
=================================

.. toctree::
   :maxdepth: 2

   installation
   help
   internals
   contributing
   license


--------------------------------

.. include:: ../README.rst


Indices and tables
~~~~~~~~~~~~~~~~~~

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
