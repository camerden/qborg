Contributing
============

This page will provide you with all kinds of information that you need to get
started with the development of QBorg and provide you with some information on
how we work.

Developers
----------

TODO


Translators
-----------

Translators are important to the QBorg project as they ensure that the
application is usable by many people not familiar with the English language.
If you encountered a spelling error in QBorg or want it to speak your language,
this is the section for you.

.. toctree::
   :titlesonly:

   contributing/translators/update_translations

Packagers
---------

TODO
