====
Help
====

To create a backup, you must first create a repository. The repository serves as the storage location for the backups.



Creating a repository
---------------------

.. image:: /img/fresh_start.png
Open the dialog via the menu *File-Create Repository...* or the button *create Repository*
*QBorg* must be selected in the navigation area

.. image:: /img/init_repository.png
Select the repository properties

* Name: Unique name to identify the repository
* Backing Store: Type of repository. Currently only *file*
* Path: Folder path where the repository is to be created. (The folder must be empty)
* Encrypton Mode: Encryption method. If *repokey* or *keyfile* is selected, a password must be selected
    * none: No encryption
    * repokey: Encryption, key is stored in the repository
    * keyfile: Encryption, key is stored in the users home directory

Repository Actions
-------------------
.. image:: /img/repository_action.png
Select the repository in the navigation area and optionally enter the password.


The title bar contains context-related actions.

* Focus on a repository
    * Rename Repository
    * Delete Repository
    * Create Backup
    * Create Job
* Focus on backups
    * Create Backup
* Focus on a backup
    * Create Backup
    * Delete Backup
    * Restore Backup
    * Rename Backup
* Jobs
    * *Not yet implemented*

The actions are described below.

Rename Repository
^^^^^^^^^^^^^^^^^
You can use this function to rename the repository. A new name can be selected in the dialog.

Delete Repository
^^^^^^^^^^^^^^^^^
Deletes the repository. When the repository is deleted, all backups stored in it are lost.

Create Backup
^^^^^^^^^^^^^
.. image:: /img/create_backup.png
Creates a backup with the selected configuration.

1. Choose Repository: The selected repository is chosen by default
2. Previous Job as Template: *Not yet implemented*
3. New Backup Name: Name for the backup
4. Compression Mode: Different compression modes are selectable
5. Directories and Files
    * **Add Directory** Adds an entire folder to the backup
    * **Add Files** Adds individual files to the backup
    * **Remove marked Directory or File** Removes the selected entry from the list
6. Advanced
    * *Not yet implemented*

Delete Backup
^^^^^^^^^^^^^
Deletes a backup. If the backup is deleted, it cannot be restored.

Restore Backup
^^^^^^^^^^^^^^
The backup will be restored to the selected location.

Rename Backup
^^^^^^^^^^^^^
Function to rename a backup.

Create Job
^^^^^^^^^^
*Not yet implemented*



Password prompt
---------------
.. image:: /img/password_prompt.png
If an encryption method with password is selected, the repository must be unlocked to perform actions on it.
The password is cached by QBorg for a certain period of time after the first query.
This allows successive actions without re-entering the password.
