============
Installation
============

Generally QBorg should run on every system that's capable of running at least
BorgBackup version 1.1.

| For more information on BorgBackup's system requirements refer to:
| https://borgbackup.readthedocs.io/en/latest/installation.html

QBorg has been tested on these distributions:

=============== ======================= ===================
Distribution    Minimum version [#min]_ Support status
=============== ======================= ===================
`Alpine Linux`_ 3.5                     Best effort
`Arch Linux`_   --                      Best effort
`Debian`_       8                       Supported
`Gentoo`_       --                      Supported
`NetBSD`_       7.0                     Not actively tested
`OpenBSD`_      6.0                     Not actively tested
`RHEL/CentOS`_  6                       Not actively tested
`Mac OS X`_     10.4                    Best effort
`Ubuntu`_       14.04                   Best effort
`Windows WSL`_  10                      Not supported
=============== ======================= ===================

.. GNU Guix, Fedora, Mageia, NixOS, OpenIndiana, Solaris, openSUSE, ... ?


.. [#min]
   | Older versions and other distributions may work if they fulfill the `System Requirements`_.
   | Their usage is not supported, though.


System requirements
-------------------

QBorg requires a unixoid operating system with at least the following installed:

- | Python 3.4 or higher
  | Modules:

   - PyQt4
   - PyYAML
- Qt 4 or higher
- BorgBackup 1.1 or higher


Installation guides
-------------------

Alpine Linux
~~~~~~~~~~~~

#. Set up X

   First, run ``setup-xorg-base`` as root and follow the instructions on the
   terminal.

   .. todo install DE

#. Install dependencies

   Not all dependencies can be found in the main repository.
   Make sure to `configure the community repository <https://wiki.alpinelinux.org/wiki/Enable_Community_Repository>`__ before continuing.

   .. code-block:: sh

     apk add -U python3 py3-pip py3-sip

#. Install BorgBackup

   a. Alpine Linux 3.7 (or higher)

      Starting with Alpine Linux 3.7 a new-enough version of BorgBackup is available in the community repository.
      Install it using the following command:

      .. code-block:: sh

        apk add borgbackup

   b. Other

      On older versions of Alpine Linux BorgBackup needs to be installed using pip:

      .. code-block:: sh

        pip3 install borgbackup

#. Install PyQt4

   - Install the prerequisites:

     .. code-block:: sh

       apk add build-base python3-dev qt-dev py3-sip py3-sip-dev

   - | Download PyQt4 from `SourceForge <https://sourceforge.net/projects/pyqt/files/PyQt4>`__.
     | And compile it.

     .. code-block:: sh

        tar xvzf PyQt*.tar.gz
        cd PyQt*/
        python3 ./configure-ng.py --no-designer-plugin
        make
        make install


#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3 install qborg


Arch Linux
~~~~~~~~~~~~

#. Install dependencies

   .. code-block:: sh

     pacman -S python python-pyqt4 python-pip qt4 borg


#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip install qborg


Debian
~~~~~~

#. Install dependencies

   .. code-block:: sh

     apt install python3 python3-pyqt4

#. Install BorgBackup

   a. Debian 10 (buster) or higher

      .. code-block:: sh

        apt install borgbackup

   b. | Debian 9 (stretch)
      | Add the `stretch-backports repository <https://backports.debian.org/Instructions>`__ and run ``apt update`` before you continue.

      .. code-block:: sh

        apt install borgbackup

   c. Debian 8 (jessie)

      .. code-block:: sh

        pip3 install borgbackup

#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3 install qborg


Gentoo
~~~~~~

#. Install dependencies

   .. code-block:: sh

     emerge --ask $'>=dev-lang/python-3.4' 'dev-python/PyQt4' $'>=app-backup/borgbackup-1.1'

.. install pip?

#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3 install qborg


NetBSD
~~~~~~

#. Install dependencies

   .. code-block:: sh

     pkgin update
     pkgin install python36 py36-pip py36-qt4 py36-borgbackup

#. Install QBorg either from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3.6 install qborg


OpenBSD
~~~~~~~

#. Install dependencies

   .. code-block:: sh

     pkg_add python  # choose >=3.4
     pkg_add py3-pip

#. Install BorgBackup

   a. OpenBSD 6.3 (or higher)

      .. code-block:: sh

        pkg_add borgbackup

   b. OpenBSD 6.2 (or lower)

      .. code-block:: sh

        pip3.X install borgbackup #>=1.1

      .. warning::
         | On big-endian systems BorgBackup might not run because ``__builtin_bswap32`` is not defined.
         | Apply this patch manually: `patch-borg__hashindex_c <http://cvsweb.openbsd.org/cgi-bin/cvsweb/ports/sysutils/borgbackup/patches/Attic/patch-borg__hashindex_c>`_

#. Compile PyQt4 for Python 3

   - Install prerequisites

     .. code-block:: sh

       pkg_add py3-sip qt4

   - | Install PyQt4
     |
     | Determine the "correct" version of PyQt4 using ``pkg_info py-qt4``.
     | Then download it from `SourceForge <https://sourceforge.net/projects/pyqt/files/PyQt4>`_.
     | And compile it.

     .. code-block:: sh

        tar xvzf PyQt*.tar.gz
        cd PyQt*/
        python3 ./configure-ng.py --no-designer-plugin --qmake $(which qmake4) --sip $(which sip-3)
        make
        make install

#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3.X install qborg


RHEL/CentOS
~~~~~~~~~~~

#. | Configure the EPEL repository
   |
   | If you already have the EPEL repository configured, you can skip this step.

   .. code-block:: sh

     sudo yum install epel-release

#. Install dependencies

   - First install Python 3:

     .. code-block:: sh

       sudo yum install python34 python34-setuptools

   -  Then install pip:

     a. RHEL 7

        .. code-block:: sh

          sudo yum install python34-pip

     b. RHEL 6

        .. code-block:: sh

          sudo easy_install-3.4 pip

#. Install BorgBackup

   a. RHEL 7

      .. code-block:: sh

        sudo yum install borgbackup

   b. RHEL 6

      .. code-block:: sh

        sudo yum install gcc libacl-devel openssl-devel
        sudo -H pip3 install borgbackup #>=1.1

#. Install PyQt4 for Python 3

   - Install the prerequisites:

     .. code-block:: sh

       sudo yum install gcc-c++ python34-devel qt-devel

   - | Install SIP
     |
     | Determine the "correct" version of SIP using ``yum info sip``.
     | Then download it from `SourceForge <https://sourceforge.net/projects/pyqt/files/sip>`__.
     | And compile it.

     .. code-block:: sh

       tar xvzf sip*.tar.gz
       cd sip*/
       python3 ./configure.py
       make
       sudo make install
   - | Install PyQt4
     |
     | Determine the "correct" version of PyQt4 using ``yum info PyQt4``.
     | Then download it from `SourceForge <https://sourceforge.net/projects/pyqt/files/PyQt4>`__.
     | And compile it.

     .. code-block:: sh

        tar xvzf PyQt*.tar.gz
        cd PyQt*/
        python3 ./configure-ng.py --static --no-designer-plugin --qmake $(which qmake-qt4)
        make
        sudo make install

#. Install QBorg from PyPI (or `from Git`_)

   .. code-block:: sh

     pip3 install qborg


Mac OS X
~~~~~~~~

#. Install dependencies using your favourite package manager (we're going to use MacPorts here).

   .. code-block:: sh

     sudo port install python36 py36-pip py36-pyqt4
     sudo -H pip-3.6 install borgbackup #>=1.1

#. Install QBorg using pip (or `from Git`_)

   .. code-block:: sh

     sudo -H pip-3.6 install git+https://github.engineering.zhaw.ch/QBorg/QBorg.git@master

Ubuntu
~~~~~~

Refer to the instructions for `Debian`_.


Windows WSL
~~~~~~~~~~~

.. warning:: QBorg is reported to work on Windows 10 inside a Linux Subsystem
             environment. This is not supported, though.

#. Configure the Windows Subsystem for Linux

   https://docs.microsoft.com/en-us/windows/wsl/install-win10

   Choose the linux distribution of your choice (we recommend Debian or Ubuntu).

#. Install and configure an X server

   e.g. `Xming <https://sourceforge.net/projects/xming/>`_.

#. Install QBorg

   Follow the installation guide for the distribution you chose to install.

   - `Debian`_
   - `Ubuntu`_

#. Run QBorg

   #. Enter your Windows Subsystem
   #. Set the X display environment variable

      .. code-block:: sh

        export DISPLAY=:0

   #. Execute ``qborg``.


Install QBorg from Git
----------------------
.. _`from Git`:

  .. note::
    It is recommended to install QBorg from pip. Installing from Git is usually a
    bad idea because commits to the main branch are not as thoroughly tested as
    dedicated releases.

  Installing from the Git repository is just as easy as installing a regular QBorg release.

  - Install the latest version:

    .. code-block:: sh

      pip3 install git+https://github.engineering.zhaw.ch/QBorg/QBorg.git

  - Install the latest development version:

    .. code-block:: sh

      pip3 install git+https://github.engineering.zhaw.ch/QBorg/QBorg.git@develop
