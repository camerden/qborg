Generating translation files
============================

1. Run `pylupdate4` (part of PyQt4) in the `qborg/ui/pyqt4/translations` folder:
   ::
     pylupdate4 $(find .. -iname '*.py' -o -iname '*.ui') -ts de_CH.ts rm_CH.ts ...

2. Now you can open up the translation files in Qt's Linguist.
   ::
     linguist qborg/ui/pyqt4/translations/{de_CH,rm_CH,...}.ts ...

3. Compile the translation files into `*.qm` files that can be used by the
   application.
   ::
     lrelease qborg/ui/pyqt4/translations/*.ts

4. Tadaa
