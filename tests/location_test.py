import os
import tempfile
import unittest

from qborg.entities.location import Location
from qborg.logic.factories import BackingStoreAdapterFactory


class TestLocation(unittest.TestCase):
    def setUp(self):
        self.path = os.path.join(tempfile.gettempdir(), 'qborg', 'TestLocation')
        url = 'file://%s' % self.path
        self.backingstore = BackingStoreAdapterFactory.adapter_for_url(url)
        self.location = Location(self.backingstore, self.path)

        self.valid_dump = {
            'backingstore': self.backingstore.protocol,
            'path': self.path
        }

    def test_eq(self):
        assert self.location == Location(self.backingstore, self.path)

    def test_dump(self):
        assert self.valid_dump == self.location.dump()

    def test_load(self):
        assert self.location == Location.load(self.valid_dump)
