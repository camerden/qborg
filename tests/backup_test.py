import enum
import unittest
from datetime import datetime

from qborg.adapters.backingstore.file import FileBackingStoreAdapter
from qborg.adapters.backup.borg import BorgAdapter
from qborg.entities.location import Location
from qborg.entities.repository import Repository
from qborg.entities.backup import Backup
from qborg.entities.backup import BackupFile


class BorgMockEncryptionMode(enum.Enum):
    NONE = 'none'


class BorgMockAdapter(BorgAdapter):
    supported_encryption_modes = BorgMockEncryptionMode


class BackupTest(unittest.TestCase):
    def setUp(self):
        self._time = datetime.now()
        self._location = Location(FileBackingStoreAdapter(), '/tmp/qborg_test_set_location_dir')
        self._encryption_mode = BorgMockEncryptionMode.NONE
        self._repository = Repository('TestRepo', self._location, self._encryption_mode)

    def test_constructor(self):
        backup = Backup('TestBackup', self._time, self._repository)

        assert backup.name == 'TestBackup'
        assert backup.time == self._time
        assert backup.repository == self._repository

    def test_table_row_header(self):
        backup = Backup('TestBackup', self._time, self._repository)
        assert type(backup.table_row_header()) == list

    def test_eq(self):
        backup = Backup('TestBackup', self._time, self._repository)
        self.assertTrue(backup.__eq__(Backup('TestBackup', self._time, self._repository)))

    def test_str(self):
        backup = Backup('TestBackup', self._time, self._repository)
        backup_str = '[%s]: TestBackup' % self._time

        assert isinstance(str(backup), str)
        assert backup_str == str(backup)

    def test_files(self):
        backup = Backup('TestBackup', self._time, self._repository)
        bkp_file = BackupFile(True, self._time, '/some/path/to/my-file.ext', 1234, '-')

        # Set
        backup.files.append(bkp_file)
        assert backup.files == [bkp_file]
        assert 1 == len(backup.files)

        # Get
        assert backup.files[0] == bkp_file

        # Remove
        assert bkp_file in backup.files
        backup.files.remove(bkp_file)
        assert bkp_file not in backup.files


class BackupFileTest(unittest.TestCase):
    def setUp(self):
        self._time = datetime.now()
        self._location = Location(FileBackingStoreAdapter(), '/tmp/qborg_test_set_location_dir')
        self._encryption_mode = BorgMockEncryptionMode.NONE
        self._repository = Repository('TestRepo', self._location, self._encryption_mode)
        self._backup = Backup('TestBackup', self._time, self._repository)
        self._path = '/some/path/to/my-file.ext'

        self._bkp_file = BackupFile(True, self._time, self._path, 1234, '-')

    def test_constructor(self):
        bkp_file = BackupFile(True, self._time, self._path, 1234, '-')

        assert bkp_file.healthy is True
        assert bkp_file.mtime == self._time
        assert bkp_file.path == self._path
        assert bkp_file.size == 1234
        assert bkp_file.qtype == '-'

    def test_eq(self):
        bkp_file = BackupFile(True, self._time, self._path, 1234, '-')
        assert bkp_file == BackupFile(True, self._time, self._path, 1234, '-')

    def test_str(self):
        correct_str = '[%s]' % self._path
        assert isinstance(str(self._bkp_file), str)
        assert correct_str == str(self._bkp_file)

    def test_to_table_row(self):
        tr = self._bkp_file.to_table_row()
        assert list == type(tr)
        assert 4 == len(tr)
