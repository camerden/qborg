import enum

from unittest import TestCase
from unittest.mock import patch

from qborg.entities.repository import Repository
from qborg.ui.pyqt4.controllers.listrepocontroller import ListRepoController


class BorgMockEncryptionMode(enum.Enum):
    NONE = 'none'


class ListRepoTest(TestCase):

    def setUp(self):
        self._repository = Repository("test", "/test", BorgMockEncryptionMode.NONE)

        with patch('qborg.ui.pyqt4.controllers.listrepocontroller.ListRepoLogic'), \
                patch('qborg.ui.pyqt4.controllers.maincontroller.MainGui') as parent_mock:
            self.lc = ListRepoController(parent=parent_mock, repository=self._repository)

    @patch('qborg.ui.pyqt4.controllers.listrepocontroller.ListRepoLogic')
    @patch('qborg.ui.pyqt4.controllers.maincontroller.MainGui')
    def test_constructor(self, maingui_mock, logic_mock):
        c = ListRepoController(parent=maingui_mock, repository=self._repository)

        logic_mock.assert_called_once_with()  # no arguments
        c.logic.list_repository.assert_called_once_with(
            delegate=c.delegate, repository=self._repository)
        assert maingui_mock == c.parent
