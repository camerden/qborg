import enum
from datetime import datetime

from unittest import TestCase
from unittest.mock import patch

from qborg.entities.repository import Repository
from qborg.entities.backup import Backup
from qborg.ui.pyqt4.controllers.listbackupcontroller import ListBackupController


class BorgMockEncryptionMode(enum.Enum):
    NONE = 'none'


class ListBackupTest(TestCase):
    def setUp(self):
        self._time = datetime.now()
        self._repository = Repository("test", "/test", BorgMockEncryptionMode.NONE)
        self._backup = Backup('TestBackup', self._time, self._repository)

        with patch('qborg.ui.pyqt4.controllers.listbackupcontroller.ListBackupLogic'), \
                patch('qborg.ui.pyqt4.controllers.maincontroller.MainGui') as parent_mock:
            self.lc = ListBackupController(parent=parent_mock, backup=self._backup)

    @patch('qborg.ui.pyqt4.controllers.listbackupcontroller.ListBackupLogic')
    @patch('qborg.ui.pyqt4.controllers.maincontroller.MainGui')
    def test_constructor(self, maingui_mock, logic_mock):
        c = ListBackupController(parent=maingui_mock, backup=self._backup)

        logic_mock.assert_called_once_with()
        c.logic.list_backup.assert_called_once_with(delegate=c.delegate, backup=self._backup)
        assert maingui_mock == c.parent
